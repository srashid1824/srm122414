<?php
    echo "This will be my 1st commit";
    $str1 = "1st";
    $str2 = "2nd";
    $str3 = "1st V.1.1";
    $str4 = "2nd V.2.1";
?>

<!DOCTYPE html>
<html>
    <head lang="en">
        <title>Git and PHP</title>
    </head>

    <body>
        <table border="2">
            <thead>
                <tr>
                    <th>Commit Name</th>
                    <th>Commit Type</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <a href="#"><p><?php echo $str1; ?></p></a>
                    </td>
                    <td>
                        <a href="#"><p><?php echo $str2; ?></p></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="#"><p><?php echo $str3; ?></p></a>
                    </td>
                    <td>
                        <a href="#"><p><?php echo $str4; ?></p></a>
                    </td>
                </tr>

            </tbody>
        </table>
    </body>
</html>